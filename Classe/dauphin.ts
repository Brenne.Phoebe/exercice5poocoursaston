import Mammifere from "../Classe_Abstract/mammifere";
import Aquatique from '../Interface/aquatique';

export default class Dauphin extends Mammifere implements Aquatique {
    /*
     * Cette classe possède les méthodes suivantes : Display   get nom,poids, dateNaissance, set nom, poids, dateNaissance
     * de la classe Mammifère
     */
    /**
     * Methode Respirer sous de l'eau pas de return
     */
    respirerSousEau(): void {
        console.log("Regarde comme il respire sous l'eau, dire que j'avais cru enfin le noyer ... Mince !")
    }
    /**
     * Methode Nager sous de l'eau pas de return
     */
    nager(): void {
       console.log("Il ondule comme une sièrene, que c'est beau :D")
    }
    /**
     * Methode Retenir Respiration pas de return
     */
    retenirRespiration():void {
        console.log("Tu crois que c'est lui el famosso record-man d'apnée ?")
    }
    /**
     * Methode Cliquetter pas de return
     */
    cliquetter():void {
        console.log("Quel bruit un tentiné casse couille au oreilles, flipper finira un jour à la poel ....")
    }

}