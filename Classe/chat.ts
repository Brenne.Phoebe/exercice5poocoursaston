import Mammifere from "../Classe_Abstract/mammifere";
import Terrestre from '../Interface/terrestre'

export default class Chat extends Mammifere implements Terrestre{
    /*
     * Cette classe possède les méthodes suivantes : Display, get nom,poids, dateNaissance, set nom, poids, dateNaissance
     * de la classe Mammifère
     */
    /**
     * Methode Miauler pas de return
     */
    miauler(): void{
        console.log("Oh un chat qui miaule !")
    }
    /**
     * Methode Marcher pas de return
     */
    marcher(): void {
        console.log("Non de Dieu il marche !!")
    }
    /**
     * Methode Respirer hors de l'eau pas de return
     */
    respirerHorsDeLeau(): void {
        console.log("Ouf !! Il respire encore hors de l'eau :p")
    }
}