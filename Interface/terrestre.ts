import MilieuNaturel from "./milieuNaturel";

export default interface Terrestre extends MilieuNaturel {
    /**
     * Méthode Marcher, void, pas de return, permettra à l'animal de marcher
     */
    marcher(): void ;
    /**
     * Méthode Respirer Sous l'eau, void, pas de return, permettra à l'animal de repsirer hors de l'eau
     */
    respirerHorsDeLeau() : void ;
}