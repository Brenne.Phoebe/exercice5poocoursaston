import MilieuNaturel from "./milieuNaturel";

export default interface Aquatique extends MilieuNaturel {
    /**
     * Méthode Respirer Sous Eau, void, pas de return, 
     * permettra à l'animal de respier sous l'eau
     */
    respirerSousEau(): void ;
     /**
     * Méthode Respirer Sous Eau, void, pas de return, 
     * permettra à l'animal de Nager
     */
    nager(): void ;
}