export default interface Animal {

    _nom : string;
    _poids : number;
    _dateNaissance : Date;

    /**
     * Méthode Display, void, pas de return, 
     * permettra d'afficher les attribut de l'animal
     */
    display():void ;
}