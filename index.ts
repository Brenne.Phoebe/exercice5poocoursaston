import Chat from './Classe/chat';
import Dauphin from './Classe/dauphin'

const chat = new Chat('Big', 6, '2019/05/19') ;
const dauphin = new Dauphin ('Flipper', 10, '2017/06/09') ;

chat.display() ;
chat.respirerHorsDeLeau() ;
chat.marcher();
chat.miauler() ;

dauphin.display() ;
dauphin.nager() ;
dauphin.respirerSousEau() ;
dauphin.cliquetter() ;
