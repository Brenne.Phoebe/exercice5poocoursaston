import Animal from "../interface/animal"

export default abstract class Mammifere implements Animal {

    _nom : string;
    _poids : number;
    _dateNaissance : Date;

    /**
     * Constructeur de la classe Mammifère héritiaire de l'interface Animal
     * @param nom (string)
     * @param poids (number)
     * @param dateNaissance (String)
     */
    constructor(nom: string, poids: number, dateNaissance: string) {
        this._nom = nom ;
        this._poids = poids ;
        this._dateNaissance = new Date (dateNaissance) ;
    }

    /**
     * Setteur Nom attend un string
     * * @param nom (string)
     */
    set nom (nom:string) {
        this._nom = nom ;
    }
    /**
     * Setteur Poids attend un number
     * * @param poids (number)
     */
    set poids (poids:number){
        this._poids = poids ;
    }
    /**
     * Setteur dateNaissance attend une Date
     * * @param dateNaissance (Date)
     */
    set dateNaissance (dateNaissance: Date){
        this._dateNaissance = dateNaissance
    }
    /**
     * Getteur Nom return string
     * * @param nom (string)
     */
    get nom ():string{
        return this._nom ;
    }
    /**
     * Getteur Poids return number 
     * * @param poids (number)
     */
    get poids ():number{
        return this._poids ;
    }
    /**
     * Getteur dateNaissance return Date
     * * @param dateNaissance (Date)
     */
    get dateNaissance ():Date {
        return this._dateNaissance;
    }
    /**
     * Méthode d'affichage du param de l'animal (nom, poids, dateNaissance)
     */
    display():void{
        console.log(`Bonjour, je m'appelle ${this._nom}, je pèse ${this._poids}. Je suis né le ${this._dateNaissance}. Je suis un Mammifère ! Je 
        peux donc marcher() et respirerHorsDeLeau(), essai pour voir !`)
    }
}